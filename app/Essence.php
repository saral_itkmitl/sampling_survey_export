<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Essence extends Model
{
    protected $table = 'survey_kerry_003essence';
}
