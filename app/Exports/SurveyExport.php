<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Downy;
use App\Essence;
use App\Essencepowder;
use App\Mizumiuv;
use App\Pao;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SurveyExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function headings(): array
    {
        return [
             'email',
             'ans1',
             'ans2',
              'ans3',
               'ans4',
               'ans5',
               'ans6',
               'ans7',
               'ans8',
               'ans9',
               'ans10',
               'intPointsGiven',
               'CreatedDate',
               'UpdatedDate',
                'latestStep',
                'IsMember',
                'PotentialEarn',
                'PotentialEarnDate'
          ];
    }
    
    public function campaign($cam)
    {
        $this->cam = $cam;
        
        return $this;
    }

    public function collection()
    {
        // echo Downy::all();
    //    return Downy::all();
        if($this->cam == "downy"){
            return Downy::all( 'email', 'ans1', 'ans2', 'ans3', 'ans4', 'ans5', 'ans6', 'ans7', 'ans8', 'ans9', 'ans10', 'intPointsGiven', 'CreatedDate', 'UpdatedDate', 'latestStep', 'IsMember', 'PotentialEarn', 'PotentialEarnDate');
        }else if($this->cam == "essence"){
            return Essence::all( 'email', 'ans1', 'ans2', 'ans3', 'ans4', 'ans5', 'ans6', 'ans7', 'ans8', 'ans9', 'ans10', 'intPointsGiven', 'CreatedDate', 'UpdatedDate', 'latestStep', 'IsMember', 'PotentialEarn', 'PotentialEarnDate');
        }else if($this->cam == "pao"){
            return Pao::all( 'email', 'ans1', 'ans2', 'ans3', 'ans4', 'ans5', 'ans6', 'ans7', 'ans8', 'ans9', 'ans10', 'intPointsGiven', 'CreatedDate', 'UpdatedDate', 'latestStep', 'IsMember', 'PotentialEarn', 'PotentialEarnDate');
        }else if($this->cam == "essencepowder"){
            return Essencepowder::all( 'email', 'ans1', 'ans2', 'ans3', 'ans4', 'ans5', 'ans6', 'ans7', 'ans8', 'ans9', 'ans10', 'intPointsGiven', 'CreatedDate', 'UpdatedDate', 'latestStep', 'IsMember', 'PotentialEarn', 'PotentialEarnDate');
        }else if($this->cam == "mizumiuv"){
            return Mizumiuv::all( 'email', 'ans1', 'ans2', 'ans3', 'ans4', 'ans5', 'ans6', 'ans7', 'ans8', 'ans9', 'ans10', 'intPointsGiven', 'CreatedDate', 'UpdatedDate', 'latestStep', 'IsMember', 'PotentialEarn', 'PotentialEarnDate');
        }
        
    }
}