<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Downy;
use App\Essence;
use App\Essencepowder;
use App\Mizumiuv;
use Carbon\Carbon;
use App\Pao;
use App\Exports\SurveyExport;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Downy()
    {
        $now = Carbon::now()->add('hour', 7);
        return (new SurveyExport)->campaign('downy')->download('downy-export_'.$now.'.xlsx');
    }

    public function Essence()
    {
        $now = Carbon::now()->add('hour', 7);
        return (new SurveyExport)->campaign('essence')->download('essence-export_'.$now.'.xlsx');
    }
    public function Essencepowder()
    {
         $now = Carbon::now()->add('hour', 7);
        return (new SurveyExport)->campaign('essencepowder')->download('essencepowder-export_'.$now.'.xlsx');
    }
    public function Mizumiuv()
    {
         $now = Carbon::now()->add('hour', 7);
         return (new SurveyExport)->campaign('mizumiuv')->download('mizumiuv-export_'.$now.'.xlsx');
    }
    public function Pao()
    {
         $now = Carbon::now()->add('hour', 7);
        return (new SurveyExport)->campaign('pao')->download('pao-export_'.$now.'.xlsx');
    }


}
