<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});




Route::get('/downy', 'SurveyController@Downy')->name('downy');;

Route::get('/essence', 'SurveyController@Essence')->name('essence');;

Route::get('/essencepowder', 'SurveyController@Essencepowder')->name('essencepowder');;

Route::get('/mizumiuv', 'SurveyController@Mizumiuv')->name('mizumiuv');;

Route::get('/pao', 'SurveyController@Pao')->name('pao');;


// Route::get('/export', 'SurveyController@exportExcel');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
