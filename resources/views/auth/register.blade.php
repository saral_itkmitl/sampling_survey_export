@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <div class="content">
                        <div class="title m-b-md">
                            Not available yet
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
