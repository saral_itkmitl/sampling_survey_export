@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="w3-table w3-bordered">
                        <tr>
                          <th>Survey Name</th>
                          <th>Download</th>
                        </tr>
                        <tr>
                          <td>Downy</td>
                        <td><a href="{{route('downy')}}" class="w3-button w3-blue w3-border">Download</a></td>
                       
                        </tr>
                        <tr>
                          <td>Pao silvernano</td>
                         <td><a href="{{route('pao')}}" class="w3-button w3-blue w3-border">Download</a></td>
                   
                        </tr>
                        <tr>
                            <td>Essence</td>
                          <td><a href="{{route('essence')}}" class="w3-button w3-blue w3-border">Download</a></td>
                    
                        </tr>
                        <tr>
                            <td>Essence powder</td>
                           <td><a href="{{route('essencepowder')}}" class="w3-button w3-blue w3-border">Download</a></td>
                    
                        </tr>
                        <tr>
                            <td>Mizumiuv</td>
                            <td><a href="{{route('mizumiuv')}}" class="w3-button w3-blue w3-border">Download</a></td>
                    
                        </tr>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
